package com.the.sample.app.model

import jakarta.persistence.*

@Entity
@Table(name = "UserData")
class User (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "identifier")
    val id: Long? = null,
    @Column(name="fullName", length = 40, nullable = false)
    val fullName: String,
    @Column(name="email", length = 50, unique = true, nullable = false)
    val email: String
)